#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>


void main()
{
	int num, positivos = 0, negativos = 0;
	float media = 0, percent = 0, soma = 0;



	do
	{
		printf("Digite um valor diferente de 0\n");
		scanf("%i", &num);

		if (num != 0)
		{
			if (num > 0)
			{
				positivos++;
			}
			else if (num < 0)
			{
				negativos++;
			}
			soma += (float)num;
			media = soma / (float)(positivos + negativos);
			percent = (float)(100 / (positivos + negativos))*positivos;

			printf("media: %.2f\n", media);
			printf("negativos: %i\n", negativos);
			printf("positivos: %i\n", positivos);
			printf("porcentagem negativos: %.2f %\n", 100 - percent);
			printf("porcentagem positivos: %.2f %\n", percent);
		}



	} while (num != 0);

	system("pause");

}
